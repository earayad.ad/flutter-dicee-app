import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  return runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.red,
        appBar: AppBar(
          title: Text('Dicee'),
          backgroundColor: Colors.red,
        ),
        body: DicePage(),
      ),
    ),
  );
}

class DicePage extends StatefulWidget {
  @override
  State<DicePage> createState() => _DicePageState();
}

class _DicePageState extends State<DicePage> {
  var leftDiceNumber = 1;
  var rightDiceNumber = 1;

  void generateRandomNumber() {
    setState(() {
      leftDiceNumber = 1 + Random().nextInt(6);
      rightDiceNumber = 1 + Random().nextInt(6);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
          child: Row (
              children: <Widget> [
                Expanded(
                    child: TextButton(
                        style: ButtonStyle(
                          overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
                          padding: MaterialStateProperty.all<EdgeInsets>(
                            EdgeInsets.all(16.0),
                          ),
                        ),
                        onPressed: generateRandomNumber,
                        // import image dynamically regarding the value of leftDiceNumber
                        child: Image.asset('images/dice$leftDiceNumber.png')
                    )
                ),
                Expanded(
                    child: TextButton(
                        style: ButtonStyle(
                          // remove ripple effect
                          overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
                          padding: MaterialStateProperty.all<EdgeInsets>(
                            EdgeInsets.all(16.0),
                          ),
                        ),
                        onPressed: generateRandomNumber,
                        child: Image.asset('images/dice$rightDiceNumber.png')
                    )
                ),
              ]
          ),
        )
    );
  }
}
